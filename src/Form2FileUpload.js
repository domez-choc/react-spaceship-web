import $ from "jquery";
import React from 'react'

export default class Form2FileUpload extends React.Component {
	constructor(props) {
		super(props);

		this.parent = React.createRef();
	}

	componentDidMount() {
		let options2 = $.extend({
			upload_type: 'upload',
			input_name: 'file_upload',
			width: 40,
			height: 40,
			single_file_upload: false,
			removeInputName: 'remove_file_upload',
			uploadButtonName: 'อัพโหลด',
			maxFileUpload: 99999999,
			acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			isCanRemove: true
		}, this.props);

		let url = options2.url ? options2.url : '/api/file/upload?type=upload';

		switch (options2.upload_type) {
			case 'single_image':
			case 'gallery':
				let isGallery = false;
				if (options2.upload_type === 'gallery') {
					isGallery = true;
				}

				window.Global.makeUploadButton({
					singleImage: {
						css: {
							width: options2.width,
							height: options2.height
						},
						input: {
							name: options2.input_name,
							value: options2.value || false,
							url: options2.image_url || false
						},
						isGallery: isGallery
					},
					element: window.$(this.parent.current).find('#upload'),
					url: url,
					onUploadDone: (response) => {
						//console.log(response);

						// if (response.error
						// 	&& response.message
						// 	&& response.message.length > 0
						// ) {
						// 	window.Global.messageTop({
						// 		type: 'negative',
						// 		message: response.message[0]['error_message']
						// 	});
						// 	return false;
						// }
						// if (!response.result.success) {
						// 	window.Global.messageTop({
						// 		type: 'negative',
						// 		message: 'อัพโหลดไม่สำเร็จ ขนาดไฟล์ไม่เกิน 30Mb'
						// 	});
						// 	return false;
						// }

						if (response.result
							&& response.result.files.length === 1
							&& response.result.files[0].name
						) {
							window.Global.messageTop({
								message: 'อัพโหลดสำเร็จ'
							});
							// $.each(response.result.file_uploads, function (index, file) {
							// 	console.log(file);
							// });
							if (this.props.onImageUploadSuccess) {
								this.props.onImageUploadSuccess(response.result.files[0]);
							}
						}

						return true;
					}
				});
				break;
			case 'upload':
				window.Global.makeUploadButton({
					uploadFile: {
						input: {
							name: options2.input_name
						},
						textOnButton: !options2['uploadButtonName']
							? 'อัพโหลด'
							: options2['uploadButtonName'],
						showImagePreview: options2.showImagePreview || false
					},
					acceptFileTypes: options2.acceptFileTypes,
					singleFileUploads: options2.single_file_upload,
					element: window.$(this.parent.current).find('#upload'),
					url: url,
					maxFileUpload: options2.maxFileUpload,
					key_file_data: options2.key_file_data || 'file_upload',
					onUploadDone: (response) => {
						if (this.props.onUploadDone) {
							return this.props.onUploadDone(response);
						}
						return true;
					},
					onSuccess: (data) => {
						if (this.props.onSuccess) {
							return this.props.onSuccess(data);
						}
					},
					onRemove: (data) => {
						console.log(data);
					}
				});
				if (options2.items) {
					if (window.$(this.parent.current).find('.Upload_fileRoot').length === 0) {
						window.$(this.parent.current).html('<div class="Upload_fileRoot"></div>');
					}
					window.Global.getElementFileUpload({
						$parent: window.$(this.parent.current).find('.Upload_fileRoot'),
						data: options2.items,
						remove_input_name: options2.removeInputName,
						is_can_remove: options2['isCanRemove']
					});
				}
				break;
			case 'upload2':
				window.Global.makeUploadButton({
					uploadFile: {
						input: {
							name: options2.input_name
						},
						textOnButton: 'อัพโหลด',
						showImagePreview: options2.showImagePreview || false
					},
					acceptFileTypes: options2.acceptFileTypes,
					singleFileUploads: options2.single_file_upload,
					element: window.$(this.parent.current).find('#upload'),
					url: url,
					maxFileUpload: options2.maxFileUpload,
					key_file_data: options2.key_file_data || 'file_upload',
					onUploadDone: (response) => {
						if (this.props.onUploadDone) {
							return this.props.onUploadDone(response);
						}
						return true;
					},
					onSuccess: (data) => {
						if (this.props.onSuccess) {
							return this.props.onSuccess(data);
						}
					},
					onRemove: (data) => {
						console.log(data);
					}
				});

				window.$(this.parent.current).find('#upload').hide();

				break;
		}

		if (!options2['isCanRemove']) {
			window.$(this.parent.current).find('#title-upload-button').remove();
		}

		// console.log(this.parent.current);
	}

	render() {
		if (this.props.upload_type === 'upload2'
			&& this.props.data !== false
		) {
			return (
				this.props.render(this.parent, this.props.data)
			);
		}


		return (
			<div ref={this.parent}
			     className="">
				<div id="upload"></div>
				{
					this.props.data === false
						? this.props.button(this)
						: this.props.render(this.parent, this.props.data)
				}
				{this.props.children}
				{
					this.props.onRemove
						?
						<div className="link"
						     onClick={() => {
							     this.props.onRemove();
						     }}>ลบ
						</div>
						: null
				}
			</div>
		);
	}
}
