/* eslint-disable react/no-children-prop */
import React from 'react';
import {AtomicBlockUtils, EditorState, RichUtils} from 'draft-js';
import clsx from 'clsx';

export const MediaButton = () => {
	const linkPlugin = props => {

		// const selection = props.getEditorState.getSelection();
		// if (!selection.isCollapsed()) {
		// 	const contentState = props.getEditorState.getCurrentContent();
		// 	const startKey = props.getEditorState.getSelection().getStartKey();
		// 	const startOffset = props.getEditorState.getSelection().getStartOffset();
		// 	const blockWithLinkAtBeginning = contentState.getBlockForKey(startKey);
		// 	const linkKey = blockWithLinkAtBeginning.getEntityAt(startOffset);
		//
		// 	let url = '';
		// 	if (linkKey) {
		// 		const linkInstance = contentState.getEntity(linkKey);
		// 		url = linkInstance.getData().url;
		// 	}
		//
		// 	// console.log(this);
		//
		// 	this.setState({
		// 		showURLInput: true,
		// 		urlValue: url,
		// 	}, () => {
		// 		setTimeout(() => this.focus(), 0);
		// 	});
		// }

		const toggleStyle = event => {
			event.preventDefault();
			// props.setEditorState(
			// 	RichUtils.toggleInlineStyle(props.getEditorState(), style)
			// );

			let image = [];
			window.Global.confirm({
				title: 'ลิงค์',
				content: '<form class="ui content form">\n\t<div class="ui two item menu">\n\t\t<a class="item active" data-selected_type="image">Image</a>\n\t\t<a class="item" data-selected_type="youtube_embed">Youtube Embed</a>\n\t</div>\n\t<input type="hidden" name="selected_type" value="image">\n\t<div class="flex row wrap column one column computer padded">\n\t\t<div class="column field required" data-type="image">\n\t\t\t<label for="">เลือกรูป</label>\n\t\t\t<div id="upload"></div>\n\t\t</div>\n\t\t<div class="column field" data-type="image">\n\t\t\t<label for="">Link</label>\n\t\t\t<input type="text" name="href">\n\t\t</div>\n\t\t<div class="column field" data-type="youtube_embed" style="display: none;">\n\t\t\t<label for="">Youtube Link</label>\n\t\t\t<input type="text" name="youtube_embed_url">\n\t\t</div>\n\t</div>\n\t<div class="ui error message"/>\n</form>',
				cancel_text: 'ยกเลิก',
				approve_text: 'ตกลง',
				onShow: ($modal) => {
					$modal.find('.ui.menu .item').each(function () {
						window.$(this).on('click', function () {
							$modal.find('.ui.menu .item').removeClass('active');
							window.$(this).addClass('active');
							$modal.find('div[data-type]').hide();
							let selected_type = window.$(this).data().selected_type;
							$modal.find('div[data-type="' + selected_type + '"]').show();
							$modal.find('input[name="selected_type"]').val(selected_type);
						});
					});
					// $modal.find('form').form2('init', {
					// 	data: {
					// 		// href: href
					// 	}
					// });
					window.Global.makeUploadButton({
						element: $modal.find('#upload'),
						uploadFile: {
							input: {
								name: 'image_filename'
							},
							textOnButton: 'อัพโหลด',
							showImagePreview: true
						},
						singleFileUploads: false,
						url: props.editor.uploadUrl,
						onUploadDone: (response) => {
							image.push(response.result.files[0]);
							// console.log(response.result.files[0].url);
							// if (this.props.onUploadDone) {
							// 	return this.props.onUploadDone(response);
							// }
							return true;
						}
					});
				},
				onApprove: function ($modal) {
					let data = $modal.find('form').serializeObject();
					if (data.selected_type === 'image') {
						if (image.length === 0) {
							return;
						}
						const contentState = props.editor.state.editorState.getCurrentContent();
						const contentStateWithEntity = contentState.createEntity(
							"image",
							"IMMUTABLE",
							{
								src: image[0].url,
								href: data.href
							}
						);
						const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
						const newEditorState = EditorState.set(
							props.editor.state.editorState,
							{currentContent: contentStateWithEntity},
							"create-entity"
						);
						props.editor.setState(
							{
								editorState: AtomicBlockUtils.insertAtomicBlock(
									newEditorState,
									entityKey,
									" "
								)
							},
							() => {
								setTimeout(() => props.editor.focus(), 0);
							}
						);
					}
					if (data.selected_type === 'youtube_embed') {
						if (!data.youtube_embed_url) {
							return;
						}
						const contentState = props.editor.state.editorState.getCurrentContent();
						const contentStateWithEntity = contentState.createEntity(
							"MEDIA",
							"IMMUTABLE",
							{
								src: data.youtube_embed_url,
							}
						);
						const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
						const newEditorState = EditorState.set(
							props.editor.state.editorState,
							{currentContent: contentStateWithEntity},
							"create-entity"
						);
						props.editor.setState(
							{
								editorState: AtomicBlockUtils.insertAtomicBlock(
									newEditorState,
									entityKey,
									" "
								)
							},
							() => {
								setTimeout(() => props.editor.focus(), 0);
							}
						);
					}

					// const contentState2 = window.editorState.getCurrentContent();
					// contentState2.replaceEntityData(
					// 	block.getEntityAt(0),
					// 	{
					// 		src: src,
					// 		href: data.href,
					// 	}
					// );
				}
			});
		};

		const preventBubblingUp = event => {
			event.preventDefault();
		};

		// we check if this.props.getEditorstate is undefined first in case the button is rendered before the editor
		// const styleIsActive = () =>
		// 	props.getEditorState
		// 	&& props
		// 		.getEditorState()
		// 		.getCurrentInlineStyle()
		// 		.has(style);

		const {theme} = props;
		// const className = styleIsActive()
		// 	? clsx(theme.button, theme.active)
		// 	: theme.button;
		const className = "ui button";

		return (
			<div className={theme.buttonWrapper}
			     onMouseDown={preventBubblingUp}>
				{/*<button*/}
				{/*	className={className}*/}
				{/*	onClick={toggleStyle}*/}
				{/*	type="button"*/}
				{/*	// children={children}*/}
				{/*>*/}
				{/*	<i className="linkify icon"/>*/}
				{/*</button>*/}
				<div className="draftJsToolbar__buttonWrapper__1Dmqh">
					<button
						onClick={toggleStyle}
						type="button"
						// children={children}
						className="draftJsToolbar__button__qi1gf" style={{paddingTop: '0px'}}>
						<i className="image outline icon"/>
					</button>
				</div>
			</div>
		);
	};

	return linkPlugin;
};

export default MediaButton();
