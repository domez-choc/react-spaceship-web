import React from "react";
import {
	RichUtils,
	KeyBindingUtil,
	EditorState,
	CompositeDecorator, AtomicBlockUtils
} from "draft-js";

import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./editorStyles.css";

export const mediaStrategy = (contentBlock, callback, contentState) => {
	contentBlock.findEntityRanges(character => {
		const entityKey = character.getEntity();
		if (entityKey === null) return false;
		const type = contentState.getEntity(entityKey).getType();
		return type === "MEDIA"
			|| type === "slider";
	}, callback);
};

let uploadUrl = '';

class Media2 extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			id: Math.random()
		}
	}

	componentDidMount() {

	}

	render() {
		const {contentState, entityKey} = this.props;
		let data = contentState.getEntity(entityKey).getData();
		let slider1;
		let $slider1;
		let current = 0;

		if (contentState.getEntity(entityKey).getType() === 'slider') {
			return (
				<div style={{position: 'relative'}}>
					<Slider ref={(slider) => {
						if (!slider) return;
						slider1 = slider;
						$slider1 = window.$(slider.innerSlider.list);
					}}
					        afterChange={(current2) => {
						        current = current2
					        }}
					        {...{
						        dots: true,
						        infinite: true,
						        speed: 500,
						        slidesToShow: 1,
						        slidesToScroll: 1
					        }}>
						{
							data.items.map((item, i) => {
								let state = {...item};
								let onChange = () => {
									// window.editor.setReadOnly(true);
									let items = [...data.items];
									items[i] = {
										...items[i],
										title: state.title,
										description: state.description,
										src: state.src,
									};
									data.items[i] = items[i];
									contentState.replaceEntityData(
										entityKey,
										{
											...data,
											items: items,
										}
									);
								}

								let createUpload = (e) => {
									window.Global.makeUploadButton({
										element: window.$(e),
										uploadFile: {
											input: {
												name: 'image_filename'
											},
											textOnButton: 'อัพโหลดรูป',
											showImagePreview: true
										},
										singleFileUploads: false,
										url: uploadUrl,
										onUploadDone: (response) => {
											state.src = response.result.files[0].url;
											onChange();
											$slider1.find('div[data-image="' + i + '"]').css({
												background: 'url(' + response.result.files[0].url + ')'
											})
											// image.push(response.result.files[0]);
											// // console.log(response.result.files[0].url);
											// // if (this.props.onUploadDone) {
											// // 	return this.props.onUploadDone(response);
											// // }
											// return true;
										}
									});
								}

								return (
									<div key={i}>
										<div className="slider flex row"
										     data-image={i}
										     style={{
											     background: 'url(' + item.src + ')',
										     }}>
											<div className="col-lg-4 content">
												<div>
													<div ref={(e) => {
														createUpload(e);
													}}/>
												</div>
												<input spellCheck={false}
												       type="text"
												       placeholder=""
												       className="title"
												       name="title"
												       onFocus={() => {
													       window.editor.setReadOnly(true);
												       }}
												       onChange={(e) => {
													       state.title = e.target.value;
													       onChange(e);
												       }}
												       onBlur={() => {
													       window.editor.setReadOnly(false);
												       }}
												       defaultValue={item.title}
												/>
												<textarea
													spellCheck={false}
													placeholder=""
													className="description"
													name="description"
													onFocus={() => {
														window.editor.setReadOnly(true);
													}}
													onChange={(e) => {
														state.description = e.target.value;
														onChange(e);
													}}
													onBlur={() => {
														window.editor.setReadOnly(false);
													}}
													defaultValue={item.description}
												/>
											</div>
										</div>
									</div>
								)
							})
						}
					</Slider>
					<div style={{
						position: 'absolute',
						top: '1rem',
						right: 0,
					}}>
						<button type="button"
						        onClick={() => {
							        const {contentState, entityKey} = this.props;
							        let data = contentState.getEntity(entityKey).getData();
							        if (contentState.getEntity(entityKey).getType() !== 'slider') {
								        return;
							        }

							        let items = [...data.items];
							        items.push({
								        title: '',
								        description: '',
								        src: '',
							        });
							        contentState.replaceEntityData(
								        entityKey,
								        {
									        ...data,
									        items: items,
								        }
							        );

							        this.setState({
								        id: Math.random()
							        });
						        }}
						        className="ui button grey">
							เพิ่ม
						</button>
						<button type="button"
						        onClick={() => {
							        const {contentState, entityKey} = this.props;
							        let data = contentState.getEntity(entityKey).getData();

							        let items = [...data.items];
							        items.splice(current, 1);
							        contentState.replaceEntityData(
								        entityKey,
								        {
									        ...data,
									        items: items,
								        }
							        );

							        this.setState({
								        id: Math.random()
							        });
						        }}
						        className="ui button grey icon">
							<i className="trash icon"/>
						</button>
					</div>
				</div>
			)
		}

		return (
			<a
				className="media"
				href={data.src}
				rel="noopener noreferrer"
				target="_blank"
				aria-label={data.src}
			>
				<iframe id="ytplayer"
				        type="text/html"
				        width="640"
				        height="360"
				        src={data.src}
					// src="https://www.youtube.com/embed/M7lc1UVf-VE?autoplay=1&origin=http://example.com"
					    frameBorder="0"/>
				{/*{props.children}*/}
			</a>
		);
	}
}

const addMediaPluginPlugin = {
	keyBindingFn(event, {getEditorState}) {
		const editorState = getEditorState();
		const selection = editorState.getSelection();
		if (selection.isCollapsed()) {
			return;
		}
		if (KeyBindingUtil.hasCommandModifier(event) && event.which === 75) {
			return "add-media";
		}
	},
	handleKeyCommand(command, editorState, {getEditorState, setEditorState}) {
		if (command !== "add-media") {
			return "not-handled";
		}
		let media = window.prompt("Paste the media -");
		const selection = editorState.getSelection();
		if (!media) {
			setEditorState(RichUtils.toggleMedia(editorState, selection, null));
			return "handled";
		}
		const content = editorState.getCurrentContent();
		const contentWithEntity = content.createEntity("LINK", "MUTABLE", {
			url: media
		});
		const newEditorState = EditorState.push(
			editorState,
			contentWithEntity,
			"create-entity"
		);
		const entityKey = contentWithEntity.getLastCreatedEntityKey();
		setEditorState(RichUtils.toggleMedia(newEditorState, selection, entityKey));
		return "handled";
	},
	addSlider(editor, editorState) {
		uploadUrl = editor.uploadUrl;
		// console.log(editor, editorState);
		const contentState = editorState.getCurrentContent();
		const contentStateWithEntity = contentState.createEntity(
			"slider",
			"IMMUTABLE",
			{
				items: [
					{
						src: '/image.png',
						title: 'หัวข้อ',
						description: "รายละเอียด",
					},
				],
			}
		);
		const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
		const newEditorState = EditorState.set(
			editorState,
			{currentContent: contentStateWithEntity},
			"create-entity"
		);
		editor.setState(
			{
				editorState: AtomicBlockUtils.insertAtomicBlock(
					newEditorState,
					entityKey,
					" "
				)
			},
			() => {
				setTimeout(() => editor.focus(), 0);
			}
		);
	},
	decorators: [
		{
			strategy: mediaStrategy,
			component: Media2
		}
	]
};

export default addMediaPluginPlugin;
