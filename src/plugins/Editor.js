import React from "react";

import 'draft-js-alignment-plugin/lib/plugin.css';
import 'draft-js-image-plugin/lib/plugin.css';
import 'draft-js-focus-plugin/lib/plugin.css';
import 'draft-js-linkify-plugin/lib/plugin.css';
import 'draft-js-inline-toolbar-plugin/lib/plugin.css';
import 'draft-js-anchor-plugin/lib/plugin.css';
import 'draft-js-side-toolbar-plugin/lib/plugin.css';

import {
	AtomicBlockUtils,
	CompositeDecorator,
	convertFromRaw,
	convertToRaw,
	EditorState,
	Entity,
	RichUtils
} from "draft-js";
import {convertToHTML} from 'draft-convert';

import editorStyles from "./editorStyles.css";
import createFocusPlugin from "draft-js-focus-plugin";
import createResizeablePlugin from "draft-js-resizeable-plugin";
import createBlockDndPlugin from "draft-js-drag-n-drop-plugin";
import createAlignmentPlugin from "draft-js-alignment-plugin";
import createLinkifyPlugin from "draft-js-linkify-plugin";
import Editor, {composeDecorators} from "draft-js-plugins-editor";
import createImagePlugin from "draft-js-image-plugin";
import clsx from "clsx";
import createDragNDropUploadPlugin from "draft-js-drag-n-drop-plugin";
import mockUpload from "./mockUpload";
import addMediaPlugin from "./addMediaPlugin";
import createInlineToolbarPlugin, {Separator} from 'draft-js-inline-toolbar-plugin';
import MediaButton from './MediaButton';
import {
	ItalicButton,
	BoldButton,
	UnderlineButton,
	CodeButton,
	HeadlineOneButton,
	HeadlineTwoButton,
	HeadlineThreeButton,
	UnorderedListButton,
	OrderedListButton,
	BlockquoteButton,
	CodeBlockButton,
} from 'draft-js-buttons';

import createLinkPlugin from 'draft-js-anchor-plugin';
import linkStyles from './linkStyles.css';

import createSideToolbarPlugin from 'draft-js-side-toolbar-plugin';
import buttonStyles from './buttonStyles.css';
import toolbarStyles from './toolbarStyles.css';
import blockTypeSelectStyles from './blockTypeSelectStyles.css';

// Setting the side Toolbar at right position(default is left) and styling with custom theme
const sideToolbarPlugin = createSideToolbarPlugin({
	position: 'left',
	// theme: {buttonStyles, toolbarStyles, blockTypeSelectStyles}
});
const {SideToolbar} = sideToolbarPlugin;

const focusPlugin = createFocusPlugin();
const resizeablePlugin = createResizeablePlugin({
	horizontal: "relative",
	vertical: "relative",
	initialWidth: "auto"
});
const blockDndPlugin = createBlockDndPlugin();
const alignmentPlugin = createAlignmentPlugin();
const {AlignmentTool} = alignmentPlugin;
const linkifyPlugin = createLinkifyPlugin({
	// component: (props) => {
	// 	const contentState = window.editorState.getCurrentContent();
	// 	contentState.replaceEntityData(props.blockKey, { url: 'https://mail.google.com' });
	//
	// 	return <a {...props} />
	// }
});
const inlineToolbarPlugin = createInlineToolbarPlugin();
const {InlineToolbar} = inlineToolbarPlugin;

// Here's your chance to pass in a configuration object (see below).
const linkPlugin = createLinkPlugin({
	theme: linkStyles,
	placeholder: 'Url...'
});

const decorator = composeDecorators(
	resizeablePlugin.decorator,
	alignmentPlugin.decorator,
	focusPlugin.decorator,
	blockDndPlugin.decorator,
);
// console.log(decorator);

let manageImageLinkRef;
const imagePlugin = createImagePlugin({
	imageComponent: (props) => {
		const {block, className, theme = {}, ...otherProps} = props;
		// leveraging destructuring to omit certain properties from props
		const {
			blockProps, // eslint-disable-line no-unused-vars
			customStyleMap, // eslint-disable-line no-unused-vars
			customStyleFn, // eslint-disable-line no-unused-vars
			decorator, // eslint-disable-line no-unused-vars
			forceSelection, // eslint-disable-line no-unused-vars
			offsetKey, // eslint-disable-line no-unused-vars
			selection, // eslint-disable-line no-unused-vars
			tree, // eslint-disable-line no-unused-vars
			contentState,
			blockStyleFn,
			...elementProps
		} = otherProps;
		const combinedClassName = clsx(theme.image, className);
		const {src, href} = contentState.getEntity(block.getEntityAt(0)).getData();

		if (elementProps.style.float === 'left') {
			elementProps.style.marginRight = '2rem';
		}
		if (elementProps.style.float === 'right') {
			elementProps.style.marginLeft = '2rem';
		}

		let node = <img
			{...elementProps}
			src={src}
			role="presentation"
			className={combinedClassName}
			ref={(image) => {
				if (!image) return;

				image.onmouseover = () => {
					const position = {
						top:
							image.offsetTop -
							// this.toolbar.offsetHeight +
							// (selectionRect.top - editorRootRect.top) +
							2,
						left:
							image.offsetLeft +
							// (selectionRect.left - editorRootRect.left) +
							image.width / 2
							- manageImageLinkRef.offsetWidth / 2,
					};
					manageImageLinkRef.setAttribute("style", "position: absolute; top:" + position.top + "px; left:" + position.left + "px;");
					// this.setState({ position });

					window.$(manageImageLinkRef).off('click').on('click', () => {
						window.Global.confirm({
							title: 'ลิงค์',
							content: '<form class="ui content form">\n\t<div class="flex row wrap column one column computer padded">\n\t\t<div class="column field required">\n\t\t\t<label>กรอกลิงค์</label>\n\t\t\t<input type="text"\n\t\t\t\t   name="href"\n\t\t\t\t   value=""\n\t\t\t/>\n\t\t</div>\n\t</div>\n\t<div class="ui error message"/>\n</form>',
							cancel_text: 'ยกเลิก',
							approve_text: 'บันทึก',
							onShow: function ($modal) {
								$modal.find('form').form2('init', {
									data: {
										href: href
									}
								});
							},
							onApprove: function ($modal) {
								let data = $modal.find('form').serializeObject();
								if (!data.href) return;
								const contentState2 = window.editorState.getCurrentContent();
								contentState2.replaceEntityData(
									block.getEntityAt(0),
									{
										src: src,
										href: data.href,
									}
								);
							}
						});
					})
				}
				image.onmouseout = () => {
					if (window.$(manageImageLinkRef).is(':hover')) {
						return;
					}
					manageImageLinkRef.setAttribute("style", "position: absolute; top:-99999px; left:-99999px;");
				}
			}}
			alt={""}
		/>;

		return (
			node
		);
	},
	decorator: decorator
});
const dragNDropFileUploadPlugin = createDragNDropUploadPlugin({
	handleUpload: mockUpload,
	addImage: imagePlugin.addImage,
});
const plugins = [
	dragNDropFileUploadPlugin,
	blockDndPlugin,
	focusPlugin,
	alignmentPlugin,
	resizeablePlugin,
	imagePlugin,
	linkifyPlugin,
	addMediaPlugin,
	inlineToolbarPlugin,
	linkPlugin,
	sideToolbarPlugin,
];
/* eslint-disable */
const initialState = {
	"entityMap": {
		// 	"0": {
		// 		"type": "IMAGE",
		// 		"mutability": "IMMUTABLE",
		// 		"data": {
		// 			"src": "/logo192.png"
		// 		}
		// 	},
		// 	"1": {
		// 		"type": "MEDIA",
		// 		"mutability": "IMMUTABLE",
		// 		"data": {
		// 			"src": "https://www.youtube.com/embed/Ln3StjqU-Fs"
		// 		}
		// 	}
	},
	"blocks": [
		{
			"key": "9gm3s",
			"text": "หัวข้อ",
			"type": "unstyled",
			"depth": 0,
			"inlineStyleRanges": [],
			"entityRanges": [],
			"data": {}
		},
		// {
		// 	"key": "ov7r",
		// 	"text": " ",
		// 	"type": "atomic",
		// 	"depth": 0,
		// 	"inlineStyleRanges": [],
		// 	"entityRanges": [{
		// 		"offset": 0,
		// 		"length": 1,
		// 		"key": 0
		// 	}],
		// 	"data": {}
		// },
		// {
		// 	"key": "3csdf",
		// 	"text": " ",
		// 	"type": "atomic",
		// 	"depth": 0,
		// 	"inlineStyleRanges": [],
		// 	"entityRanges": [{
		// 		"offset": 0,
		// 		"length": 1,
		// 		"key": 1
		// 	}],
		// 	"data": {}
		// }, {
		// 	"key": "e23a8",
		// 	"text": "See advanced examples further down …",
		// 	"type": "unstyled",
		// 	"depth": 0,
		// 	"inlineStyleRanges": [],
		// 	"entityRanges": [],
		// 	"data": {}
		// }
	]
};

/* eslint-enable */

function findLinkEntities(contentBlock, callback) {
	contentBlock.findEntityRanges(
		(character) => {
			const entityKey = character.getEntity();
			return (
				entityKey !== null &&
				Entity.get(entityKey).getType() === 'LINK'
			)
		},
		callback
	)
}

const Link = (props) => {
	const {url} = Entity.get(props.entityKey).getData();
	// console.log(props);
	return (
		<a href={url}>{props.children}</a>
	)
}

const Image = props => {
	const {height, src, width} = props.contentState
		.getEntity(props.entityKey)
		.getData();
	console.log(src);
	return <img src={src} height={height} width={width}/>;
};

const decorators = [
	{
		strategy: findLinkEntities,
		component: Link,
	},
	// {
	// 	strategy: findImageEntities,
	// 	component: Image
	// }
];

function findImageEntities(contentBlock, callback, contentState) {
	contentBlock.findEntityRanges(character => {
		const entityKey = character.getEntity();
		return (
			entityKey !== null &&
			contentState.getEntity(entityKey).getType() === "IMAGE"
		);
	}, callback);
}

// const decorators = [
// 	// {
// 	// 	strategy: findImageEntities,
// 	// 	component: Image
// 	// }
// ];

const styles = {
	root: {
		fontFamily: '\'Georgia\', serif',
		padding: 20,
		width: 600,
	},
	buttons: {
		marginBottom: 10,
	},
	urlInputContainer: {
		marginBottom: 10,
	},
	urlInput: {
		fontFamily: '\'Georgia\', serif',
		marginRight: 10,
		padding: 3,
	},
	editor: {
		border: '1px solid #ccc',
		cursor: 'text',
		minHeight: 80,
		padding: 10,
	},
	button: {
		marginTop: 10,
		textAlign: 'center',
	},
	link: {
		color: '#3b5998',
		textDecoration: 'underline',
	},
};

class HeadlinesPicker extends React.Component {
	componentDidMount() {
		setTimeout(() => {
			window.addEventListener('click', this.onWindowClick);
		});
	}

	componentWillUnmount() {
		window.removeEventListener('click', this.onWindowClick);
	}

	onWindowClick = () =>
		// Call `onOverrideContent` again with `undefined`
		// so the toolbar can show its regular content again.
		this.props.onOverrideContent(undefined);

	render() {
		const buttons = [HeadlineOneButton, HeadlineTwoButton, HeadlineThreeButton];
		return (
			<div>
				{buttons.map((Button, i) => // eslint-disable-next-line
					<Button key={i} {...this.props} />
				)}
			</div>
		);
	}
}

class HeadlinesButton extends React.Component {
	// When using a click event inside overridden content, mouse down
	// events needs to be prevented so the focus stays in the editor
	// and the toolbar remains visible  onMouseDown = (event) => event.preventDefault()
	onMouseDown = (event) => event.preventDefault()

	onClick = () =>
		// A button can call `onOverrideContent` to replace the content
		// of the toolbar. This can be useful for displaying sub
		// menus or requesting additional information from the user.
		this.props.onOverrideContent(HeadlinesPicker);

	render() {
		return (
			<div onMouseDown={this.onMouseDown} className="draftJsToolbar__buttonWrapper__1Dmqh">
				<button onClick={this.onClick} className="draftJsToolbar__button__qi1gf" style={{paddingTop: '0px'}}>
					H
				</button>
			</div>
		);
	}
}

export default class ZeroWorkEditor extends React.Component {
	constructor(props) {
		super(props);

		// console.log(props);

		this.state = {
			editorState: EditorState.createWithContent(convertFromRaw(!props.content_state ? initialState : props.content_state), new CompositeDecorator(decorators)),
			// editorState: EditorState.createWithContent(convertFromRaw(initialState)),
		}
		this.promptForLink = this._promptForLink.bind(this);
		this.onURLChange = (e) => this.setState({urlValue: e.target.value});
		this.confirmLink = this._confirmLink.bind(this);
		this.onLinkInputKeyDown = this._onLinkInputKeyDown.bind(this);
		this.removeLink = this._removeLink.bind(this);

		this.uploadUrl = props.uploadUrl
			? props.uploadUrl
			: '/api/file/upload?type=upload';

		window.editorState = this.state.editorState;
	}

	onChange = (editorState) => {
		this.setState({
			editorState,
		});
		window.editorState = editorState;
		// const contentState = editorState.getCurrentContent();
		// console.log('content state', convertToRaw(contentState));
	};

	focus = () => {
		this.editor.focus();
	};

	handleKeyCommand = command => {
		const newState = RichUtils.handleKeyCommand(
			this.state.editorState,
			command
		);
		if (newState) {
			this.onChange(newState);
			return "handled";
		}
		return "not-handled";
	};

	onURLChange = e => this.setState({urlValue: e.target.value});

	onAddImage = e => {
		e.preventDefault();
		const editorState = this.state.editorState;
		const urlValue = window.prompt("Paste Image Link");
		const contentState = editorState.getCurrentContent();
		const contentStateWithEntity = contentState.createEntity(
			"image",
			"IMMUTABLE",
			{src: urlValue}
		);
		const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
		const newEditorState = EditorState.set(
			editorState,
			{currentContent: contentStateWithEntity},
			"create-entity"
		);
		this.setState(
			{
				editorState: AtomicBlockUtils.insertAtomicBlock(
					newEditorState,
					entityKey,
					" "
				)
			},
			() => {
				setTimeout(() => this.focus(), 0);
			}
		);
	};

	onUnderlineClick = () => {
		this.onChange(
			RichUtils.toggleInlineStyle(this.state.editorState, "UNDERLINE")
		);
	};

	onBoldClick = () => {
		this.onChange(RichUtils.toggleInlineStyle(this.state.editorState, "BOLD"));
	};

	onItalicClick = () => {
		this.onChange(
			RichUtils.toggleInlineStyle(this.state.editorState, "ITALIC")
		);
	};

	_promptForLink(e) {
		e.preventDefault();
		const {editorState} = this.state;
		const selection = editorState.getSelection();
		if (!selection.isCollapsed()) {
			const contentState = editorState.getCurrentContent();
			const startKey = editorState.getSelection().getStartKey();
			const startOffset = editorState.getSelection().getStartOffset();
			const blockWithLinkAtBeginning = contentState.getBlockForKey(startKey);
			const linkKey = blockWithLinkAtBeginning.getEntityAt(startOffset);

			let url = '';
			if (linkKey) {
				const linkInstance = contentState.getEntity(linkKey);
				url = linkInstance.getData().url;
			}

			// console.log(this);

			this.setState({
				showURLInput: true,
				urlValue: url,
			}, () => {
				setTimeout(() => this.focus(), 0);
			});
		}
	}

	_confirmLink(e) {
		e.preventDefault();
		const {editorState, urlValue} = this.state;
		const contentState = editorState.getCurrentContent();
		const contentStateWithEntity = contentState.createEntity(
			'LINK',
			'MUTABLE',
			{url: urlValue}
		);
		const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
		const newEditorState = EditorState.set(editorState, {currentContent: contentStateWithEntity});
		this.setState({
			editorState: RichUtils.toggleLink(
				newEditorState,
				newEditorState.getSelection(),
				entityKey
			),
			showURLInput: false,
			urlValue: '',
		}, () => {
			setTimeout(() => this.editor.focus(), 0);
		});
	}

	_onLinkInputKeyDown(e) {
		if (e.which === 13) {
			this._confirmLink(e);
		}
	}

	_removeLink(e) {
		e.preventDefault();
		const {editorState} = this.state;
		const selection = editorState.getSelection();
		if (!selection.isCollapsed()) {
			this.setState({
				editorState: RichUtils.toggleLink(editorState, selection, null),
			});
		}
	}

	getContent() {
		return convertToRaw(window.editorState.getCurrentContent())
	}

	renderView() {
		let contentState = EditorState.createWithContent(
			convertFromRaw(this.props.content)
		).getCurrentContent();
		// console.log(convertFromRaw(this.props.content));
		// console.log(EditorState.createWithContent(convertFromRaw(this.props.content)).getCurrentContent());

		let data_count = 0;

		return (
			<div dangerouslySetInnerHTML={{
				__html: convertToHTML({
					// styleToHTML: (style) => {
					// 	if (style === 'BOLD') {
					// 		return <span style={{color: 'blue'}} />;
					// 	}
					// },
					blockToHTML: (block) => {
						if (block.type === 'PARAGRAPH') {
							return <p/>;
						}
						if (block.type === 'unstyled' && !block.text) {
							return <p><br/></p>;
						}
					},
					entityToHTML: (entity, originalText) => {
						let data = entity.data;
						entity.type = entity.type.toLowerCase();

						// console.log(entity.type, originalText);

						if (entity.type === 'link') {
							return <a href={entity.data.url}>{originalText}</a>;
						}

						if (entity.type === 'image') {
							if (!data.width) data.width = 40;
							if (!data.height) data.height = 40;
							// alignment
							// href
							return (
								<img alt='image'
								     src={data.src}
								     style={{
									     maxWidth: '100%',
									     width: data.width + '%',
									     height: data.height + '%',
								     }}/>
							)
						}

						if (entity.type === 'media') {
							return (
								<iframe
									frameBorder='0'
									src={data.src}
									style={{
										width: '80%',
										height: '430px',
									}}/>
							)
						}

						if (entity.type === 'slider') {
							window.data[data_count] = data;
							return (
								<div id={"test-" + data_count}/>
							)
						}

						if (!originalText) {
							return <br/>
						}

						return originalText;
					}
				})(contentState)
			}}/>
		)
	}

	render() {
		if (this.props.mode === 'view') {
			return this.renderView();
		}

		let urlInput;
		if (this.state.showURLInput) {
			urlInput =
				<div style={styles.urlInputContainer}>
					<input
						onChange={this.onURLChange}
						ref="url"
						style={styles.urlInput}
						type="text"
						value={this.state.urlValue}
						onKeyDown={this.onLinkInputKeyDown}
					/>
					<button onMouseDown={this.confirmLink}>
						Confirm
					</button>
				</div>;
		}

		return (
			<div style={{position: 'relative'}}>
				<div style={{marginTop: '2rem'}}></div>
				{/*<div className="menuButtons">*/}
				{/*<button onClick={this.onUnderlineClick}>U</button>*/}
				{/*<button onClick={this.onBoldClick}>*/}
				{/*	<b>B</b>*/}
				{/*</button>*/}
				{/*<button onClick={this.onItalicClick}>*/}
				{/*	<em>I</em>*/}
				{/*</button>*/}
				{/*<button className="inline styleButton"*/}
				{/*        onClick={this.onAddImage}>*/}
				{/*	<i*/}
				{/*		className="material-icons"*/}
				{/*		style={{*/}
				{/*			fontSize: "16px",*/}
				{/*			textAlign: "center",*/}
				{/*			padding: "0px",*/}
				{/*			margin: "0px"*/}
				{/*		}}*/}
				{/*	>*/}
				{/*		image*/}
				{/*	</i>*/}
				{/*</button>*/}
				{/*<button*/}
				{/*	onMouseDown={this.promptForLink}*/}
				{/*	style={{marginRight: 10}}>*/}
				{/*	Add Link*/}
				{/*</button>*/}
				{/*<button onMouseDown={this.removeLink}>*/}
				{/*	Remove Link*/}
				{/*</button>*/}
				{/*</div>*/}
				{urlInput}
				{/*<button onMouseDown={() => {*/}
				{/*	console.log(addMediaPlugin.addSlider(this, this.state.editorState));*/}
				{/*}}>*/}
				{/*	TEST*/}
				{/*</button>*/}
				<div className={editorStyles.editor}
				     onClick={this.focus}>
					<Editor
						editorState={this.state.editorState}
						onChange={this.onChange}
						// readOnly={this.props.mode === 'view'}
						plugins={plugins}
						decorators={decorators}
						ref={(element) => {
							window.editor = this.editor = element;
						}}
						// toolbarClassName="toolbarClassName"
						// wrapperClassName="wrapperClassName"
						// editorClassName="editorClassName"
						// toolbar={{
						// 	inline: {inDropdown: true},
						// 	list: {inDropdown: true},
						// 	textAlign: {inDropdown: true},
						// 	link: {inDropdown: true},
						// 	history: {inDropdown: true},
						// 	// image: { uploadCallback: this._uploadImageCallBack },
						// 	// inputAccept: 'application/pdf,text/plain,application/vnd.openxmlformatsofficedocument.wordprocessingml.document,application/msword,application/vnd.ms-excel'
						// }}
					/>
					<InlineToolbar>
						{
							// may be use React.Fragment instead of div to improve perfomance after React 16
							(externalProps) => (
								<div>
									<BoldButton {...externalProps} />
									<ItalicButton {...externalProps} />
									<UnderlineButton {...externalProps} />
									<CodeButton {...externalProps} />
									<Separator {...externalProps} />
									<HeadlinesButton {...externalProps} />
									<UnorderedListButton {...externalProps} />
									<OrderedListButton {...externalProps} />
									<BlockquoteButton {...externalProps} />
									<CodeBlockButton {...externalProps} />
									<MediaButton {...externalProps}
									             editor={this}
									/>
									<linkPlugin.LinkButton {...externalProps} />
								</div>
							)
						}
					</InlineToolbar>
					<SideToolbar>
						{
							// may be use React.Fragment instead of div to improve perfomance after React 16
							(externalProps) => (
								<div>
									<HeadlineOneButton {...externalProps} />
									<HeadlineTwoButton {...externalProps} />
									<UnorderedListButton {...externalProps} />
									<OrderedListButton {...externalProps} />
									{/*<BlockquoteButton {...externalProps} />*/}
									{/*<CodeBlockButton {...externalProps} />*/}
									<MediaButton {...externalProps}
									             editor={this}
									/>
									<div className={externalProps.theme.buttonWrapper}
									     onMouseDown={(event => event.preventDefault())}>
										<div className="draftJsToolbar__buttonWrapper__1Dmqh">
											<button
												onClick={() => {
													addMediaPlugin.addSlider(this, this.state.editorState);
												}}
												type="button"
												className="draftJsToolbar__button__qi1gf"
												style={{paddingTop: '0px'}}>
												<i className="clone outline icon"/>
											</button>
										</div>
									</div>
								</div>
							)
						}
					</SideToolbar>
					<AlignmentTool/>
					{
						this.props.onSubmit
							?
							<div style={{marginTop: '2rem'}}>
								<button className="ui button fluid green"
								        type="button"
								        onClick={() => {
									        const contentState = window.editorState.getCurrentContent();
									        console.log('content state', convertToRaw(contentState));
									        this.props.onSubmit(contentState);
								        }}>
									Save
								</button>
							</div>
							: null
					}
				</div>
				<div style={{position: 'absolute', top: '-9999px', left: '-9999px'}}
				     ref={(e) => {
					     manageImageLinkRef = e;
				     }}>
					<button className="ui button icon"><i className="linkify icon"/></button>
				</div>
			</div>
		)
	}
}
