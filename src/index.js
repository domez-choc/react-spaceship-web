import React, {useEffect, useState} from 'react'
import _ from 'lodash';
// import $ from 'jquery'
// import ReactDOM from "react-dom";
// import * as serviceWorker from "./serviceWorker";
// import App from "../src/App";
// import ZeroWorkEditor from "./plugins/Editor";
import Form2FileUpload from "./Form2FileUpload";
import {createRoot} from 'react-dom/client';
import App from "./App";
// import App from "./App";

// function component() {
// 	const element = document.createElement('div');
//
// 	// Lodash, now imported by this script
// 	element.innerHTML = _.join(['Hello', 'webpack'], ' ');
//
// 	console.log(Form2FileUpload);
//
// 	return element;
// }
// document.body.appendChild(component());

// alert('ddd')
// import {createRoot} from 'react-dom/client';
// const container = document.getElementById('root');
// const root = createRoot(container); // createRoot(container!) if you use TypeScript
// root.render(<App/>);

class DownloadReportModal extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div>
				<Modal onApprove={() => false}
				       closable={false}
				       onDeny={() => {
					       this.ajax.abort();
				       }}
				       onHide={() => {
					       this.ajax.abort();
				       }}
				       onShow={(ref) => {
					       this.$modal = ref;
					       this.ajax = window.Global.ajax({
						       method: 'GET',
						       url: this.props.url,
						       data: this.props.data,
						       done: (response) => {
							       if (!response.success) {
								       return false;
							       }
							       window.$('body').append('<iframe src="' + response['download_url'] + '" style="width: 0;height: 0;"></iframe>');
							       window.$(this.$modal).modal('hide');
						       },
						       fail: () => {

						       }
					       });
				       }}
				>
					<div className="header">กำลังเตรียมรายงานสำหรับคุณ</div>
					<div className="content">
						<form ref={e => this.$form = e} className="ui form">
							<div className="ui indicating progress active" data-percent="100">
								<div className="bar" style={{width: '100%'}}/>
								<div className="label">กำลังโหลด ไม่ต้องปิดหน้าต่าง...</div>
							</div>
							<div className="ui error message"/>
						</form>
					</div>
				</Modal>
			</div>
		);
	}
}

(function ($) {
	$.fn.ssModal = function (action, option) {
		let _this = this;
		let $this = $(this);
		_this.options = {};
		this.init = function (_options) {
			_this.options = jQuery.extend({
				decimal: true,
				allow_negative: false,
				maximumDecimal: 2
			}, _options);

			console.log(_this);
			console.log($this);
			console.log(_this.options);

			return this;
		};
		this.hide = function () {
			$this.remove();
			return this;
		};
		if (action && !option) {
			return this.init(action);
		} else {
			switch (action) {
				case 'hide':
					// this.init(action);
					this.hide();
					break;
				default:
					return this.init(option);
			}
		}
	};
})(window.jQuery);

class Modal extends React.Component {
	constructor(props) {
		super(props);
		this.parent = React.createRef();
		this.state = {
			confirmModal: null
		}
	}

	componentDidMount() {
		if (this.props.init) {
			this.props.init(this.parent, this);
		}

		// if (this.props.deny) {
		// 	window.$(this.parent.current).find('button.deny').on('click', () => {
		// 		let d = {};
		// 		this.props.deny.key.forEach((value) => {
		// 			d[value] = null;
		// 		});
		// 		this.props.deny._this.setState(d);
		// 	});
		// }

		if (this.props.version
			&& this.props.version === 1
		) {
			if (this.props.onApprove) {
				window.$(this.parent.current).find('button.approve').on('click', () => {
					if (this.props.confirm) {
						this.confirmModal();
					} else {
						this.props.onApprove(this.parent, this);
					}
				});
			}
			if (this.props.onDeny) {
				window.$(this.parent.current).find('button.deny').on('click', () => {
					this.props.onDeny(this.parent, this);
				});
			}
		}
	}

	confirmModal() {
		this.setState({
			confirmModal: (
				<Modal version={1}
				       header={null}
				       headerText={'ยืนยันนัดหมาย'}
				       onDeny={() => {
					       this.setState({
						       confirmModal: null
					       });
				       }}
				       onApprove={() => {
					       this.props.onApprove(this.parent, this);
				       }}
				       button={['ยกเลิก', 'ตกลง']}
				>
					<svg aria-hidden="true" className="mx-auto mb-4 text-gray-400 w-14 h-14 dark:text-gray-200"
					     fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
						      d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path>
					</svg>
					<h3
						className="mb-5 text-lg font-normal text-center text-gray-500 dark:text-gray-400">{this.props.confirm}</h3>
				</Modal>
			)
		})
	}

	render() {
		if (this.props.version
			&& this.props.version === 1
		) {
			return (
				<div
					className="ss-modal fixed top-0 left-0 right-0 z-50 w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%)] md:h-full justify-center items-center flex bg-gray-900 bg-opacity-50 dark:bg-opacity-80 inset-0 z-40"
					aria-modal="true"
					ref={this.parent}
					role="dialog">
					<div className="relative w-full h-full max-w-lg md:h-auto">
						<div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
							{
								typeof this.props.header === 'undefined'
								&&
								<div
									className="flex items-center justify-between p-5 border-b rounded-t dark:border-gray-600">
									<h3 className="text-xl font-bold text-gray-900 dark:text-white">
										{this.props.headerText}
									</h3>
									<button type="button"
									        className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
									        data-modal-hide="medium-modal">
										<svg aria-hidden="true" className="w-5 h-5" fill="currentColor"
										     viewBox="0 0 20 20"
										     xmlns="http://www.w3.org/2000/svg">
											<path fillRule="evenodd"
											      d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
											      clipRule="evenodd"></path>
										</svg>
										<span className="sr-only">ปิด</span>
									</button>
								</div>
							}
							<div className="p-6 space-y-6">
								{/*<p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">*/}
								{/*	With less than a month to go before the European Union enacts new consumer privacy*/}
								{/*	laws for its citizens, companies around the world are updating their terms of*/}
								{/*	service agreements to comply.*/}
								{/*</p>*/}
								{/*<p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">*/}
								{/*	The European Union’s General Data Protection Regulation (G.D.P.R.) goes into effect*/}
								{/*	on May 25 and is meant to ensure a common set of data rights in the European Union.*/}
								{/*	It requires organizations to notify users as soon as possible of high-risk data*/}
								{/*	breaches that could personally affect them.*/}
								{/*</p>*/}
								{this.props.children}
							</div>
							{
								!this.props.footer
								&&
								<div
									className="flex items-center justify-end p-6 space-x-2 border-t border-gray-200 rounded-b dark:border-gray-600">
									<button type="button"
									        className="deny text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-gray-200 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600">
										{this.props.button[0]}
									</button>
									{
										this.props.button.length > 1
										&&
										<button type="button"
										        className={"approve text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"}>
											<svg className="animate-spin -ml-1 mr-3 h-5 w-5 text-white"
											     xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
												<circle className="opacity-25" cx="12" cy="12" r="10"
												        stroke="currentColor"
												        strokeWidth="4"></circle>
												<path className="opacity-75" fill="currentColor"
												      d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
											</svg>
											{this.props.button[1]}
										</button>
									}
								</div>
							}
						</div>
					</div>
					{this.state.confirmModal}
				</div>
			)
		}

		return (
			<div ref={(ref) => {
				window.$(ref).modal({
					closable: this.props.closable === undefined ? true : this.props.closable,
					autofocus: this.props.autofocus === undefined ? false : this.props.autofocus,
					observeChanges: this.props.observeChanges === undefined ? true : this.props.observeChanges,
					onShow: () => {
						if (this.props.onShowToSetupForm) {
							return this.props.onShowToSetupForm(window.$(ref));
						}
						if (this.props.onShow) {
							return this.props.onShow(ref);
						}
					},
					onApprove: ($element) => {
						if (this.props.onShowToSetupForm) {
							window.$(ref).find('form').trigger('submit');
							return false;
						}
						if (this.props.onApprove) {
							let $button = window.$(ref).find('.approve.button');
							if ($button.hasClass('loading')) return false;
							$button.addClass('loading');
							return this.props.onApprove(ref, $element);
						}
					},
					onDeny: ($element) => {
						if (this.props.onDeny) {
							return this.props.onDeny(ref, $element);
						}
					},
					onHide: ($element) => {
						setTimeout(function () {
							window.$(ref).remove();
						}, 1500);
						if (this.props.onHide) {
							return this.props.onHide(ref, $element);
						}
					}
				}).modal('show');
			}}
			     className={!this.props.className ? "ui tiny modal" : this.props.className}>
				<i className="close icon"/>
				{this.props.children}
			</div>
		);
	}
}

class AddressForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			title: this.props.title,
			type: this.props.type,
			payment: this.props.payment,
		}
	}

	setValue() {
		let type = this.state.type;
		let relationship = '';
		let branch_name = '';
		let branch_code = '';
		let bill_line_1 = '';
		let bill_line_2 = '';
		let options = {};
		let $parent = {};
		let relationshipBranceType = {};
		let $editBuyerInformationButton = {};
		let $editSellerInformationButton = {};

		this.$parent.find('input[name="' + type + '_company_name"]').val(relationship['name']);
		this.$parent.find('input[name="' + type + '_branch_name"]').val(branch_name);
		this.$parent.find('input[name="' + type + '_branch_code"]').val(branch_code);
		this.$parent.find('input[name="' + type + '_bill_line_1"]').val(bill_line_1);
		this.$parent.find('input[name="' + type + '_bill_line_2"]').val(bill_line_2);
		this.$parent.find('input[name="' + type + '_tax_number"]').val(relationship['tax_number']);
		window.GlobalFunc.setManageAddressButton2({
			$node: options.$node
		});

		if (options.onSuccess) {
			options.onSuccess();
		}
		window.GlobalFunc.selectedRelationshipToSetupAddress($parent, {
			relationship: relationship,
			type: relationshipBranceType,
			$node: this.state.payment.type === 'receive_advance_payment' ? $editBuyerInformationButton : $editSellerInformationButton
		});
	}

	renderBuyer() {
		return (
			<div className="column field">
				{
					this.state.title ?
						<label style={{display: 'inline-block', marginRight: 5}}>{this.state.title}</label>
						:
						null
				}
				{this.state.payment.buyer_company_name}
				{
					this.state.payment.buyer_branch_name
						? ' สาขา ' + this.state.payment.buyer_branch_name
						: null
				}
				{
					this.state.payment.buyer_bill_line_1
						?
						<div>{this.state.payment.buyer_bill_line_1}</div>
						: null
				}
				{
					this.state.payment.buyer_bill_line_2
						?
						<div>{this.state.payment.buyer_bill_line_2}</div>
						: null
				}
				{
					this.state.payment.buyer_tax_number
						?
						<div>เลขประจำตัวผู้เสียภาษี {this.state.payment.buyer_tax_number}</div>
						: null
				}
			</div>
		);
	}

	renderSeller() {
		return (
			<div className="column field">
				{
					this.state.title ?
						<label style={{display: 'inline-block', marginRight: 5}}>{this.state.title} </label>
						:
						null
				}
				{this.state.payment.seller_company_name}
				{
					this.state.payment.seller_branch_name
						? ' สาขา ' + this.state.payment.seller_branch_name
						: null
				}
				{
					this.state.payment.seller_bill_line_1
						?
						<div>{this.state.payment.seller_bill_line_1}</div>
						: null
				}
				{
					this.state.payment.seller_bill_line_2
						?
						<div>{this.state.payment.seller_bill_line_2}</div>
						: null
				}
				{
					this.state.payment.seller_tax_number
						?
						<div>เลขประจำตัวผู้เสียภาษี {this.state.payment.seller_tax_number}</div>
						: null
				}
			</div>
		);
	}

	render() {
		if (!this.state.mode) {
			if (this.state.type === 'buyer') {
				return this.renderBuyer()
			}
			if (this.state.type === 'seller') {
				return this.renderSeller()
			}
		}

		return (
			<div className="column field"
			     ref={(ref) => {
				     this.$parent = window.$(ref);
			     }}
			     id="editSellerInformationButton">
				<div className="flex row">
					<p className="SaleOrder_address"
					   data-title="ผู้รับเงิน">-</p>
				</div>
				<input type="hidden" name="seller_company_name" data-company_name/>
				<input type="hidden" name="seller_branch_name" data-branch_name/>
				<input type="hidden" name="seller_branch_code" data-branch_code/>
				<input type="hidden" name="seller_bill_line_1" data-bill_line_1/>
				<input type="hidden" name="seller_bill_line_2" data-bill_line_2/>
				<input type="hidden" name="seller_tax_number" data-tax_number/>
			</div>
		);
	}
}

// ReactDOM.render(
// 	<React.StrictMode>
// 		<App/>
// 	</React.StrictMode>,
// 	document.getElementById('root')
// );
//
// // If you want your app to work offline and load faster, you can change
// // unregister() to register() below. Note this comes with some pitfalls.
// // Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();

// if (!process.env.NODE_ENV
// 	|| process.env.NODE_ENV === 'development'
// ) {
// 	// dev code
// 	ReactDOM.render(<App/>, document.getElementById('root'));
// } else {
// 	// production code
// }

// ReactDOM.render(<App/>, document.getElementById('root'));

export function ModalX2({test}) {
	useEffect(() => {
		if (test) {
			alert(test);
		}
	}, []);

	return (
		<div>zzz</div>
	);
}

// const Components = {
// 	ModalX2: ModalX2,
// };
//
// export const StringComponent = ({ name, Fallback = undefined, ...rest }) => {
// 	const Component = Components[name];
//
// 	// return fallback if the component doesn't exist
// 	if (!Component) return <Fallback/>
//
// 	return <Component {...rest}/>;
// };
// console.log(StringComponent({
// 	name: 'ModalX2',
// 	test: 'xxx'
// }));

export const Render = (container, Component, {Fallback = undefined, ...rest}) => {
	const root = createRoot(container);

	// const Component2 = Component;

	// return fallback if the component doesn't exist
	if (!Component) return <Fallback/>

	root.render(<Component {...rest}/>);
}
// Render(ModalX2, {
// 	test: '555'
// })

export {DownloadReportModal, Modal, Form2FileUpload, AddressForm};
